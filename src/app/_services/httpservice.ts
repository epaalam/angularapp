import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/retry';
import { DashboardRequest } from '../_models/dashboard_request';

@Injectable()
export class HttpService {
  private readonly BASE_DOMAIN = 'http://localhost:3000';
  private  readonly SUB_DOMAIN = 'https://epallam-api.herokuapp.com';
  private readonly getAllRequestsUrl = this.BASE_DOMAIN + '/request/getAllRequest';
  private readonly signUpUrl = this.SUB_DOMAIN + '/users/signUp';
  private readonly createRequestUrl = this.SUB_DOMAIN + '/request/addRequest';

  constructor(private httpClient: HttpClient) {
  }

  getAllRequests(): Observable<DashboardRequest[]> {
    return this.httpClient.get<DashboardRequest[]>(this.getAllRequestsUrl).retry(3);
  }

  signUp(registrationJson) {
    return this.httpClient.post(this.signUpUrl, registrationJson).retry(3);
  }

  createRequest(requestJson) {
    return this.httpClient.post(this.createRequestUrl, requestJson).retry(3);
  }
}
