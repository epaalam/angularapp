import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NGXLogger} from 'ngx-logger';
import {HttpService} from '../_services/httpservice';

@Component({
  selector: 'app-raise-request',
  templateUrl: './raise-request.component.html',
  styleUrls: ['./raise-request.component.css']
})
export class RaiseRequestComponent implements OnInit {
  requestForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error: string;
  success: string;
  ccode = 91;
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private  logger: NGXLogger,
    private httpService: HttpService
  ) { }

  ngOnInit() {
    this.requestForm = this.formBuilder.group({
      beneficiary : ['', Validators.required],
      requestDetails: ['', Validators.required],
      fieldContactNumber: ['', Validators.required],
      raisedBy: ['', Validators.required],
      requestType: ['Select'],
      modeOfContact: ['Phone'],
      fieldContactMode: ['Phone'],
      contactNumber: ['', Validators.required],
    });
    this.error = null;
    this.success = null;
  }
  get formValidation() {
    return this.requestForm.controls;
  }

  directToLogin(){
    this.router.navigate(['/login']);
  }

  onSubmit() {
    console.log('Form Submitted');
    this.submitted = true;
    this.error = null;
    this.success = null;
    if (this.requestForm.invalid) {
      this.error = 'Please fill the required details';
      return;
    }
    if (this.formValidation.requestType.value === 'Select') {
      this.error = 'Please select the request type';
      return;
    }
    const request = {
      action: 'create',
      beneficiaries: this.formValidation.beneficiary.value,
      requestDetails: this.formValidation.requestDetails.value,
      requestType: this.formValidation.requestType.value,
      phoneNumber: this.formValidation.fieldContactNumber.value,
      preferredContactMode: [this.formValidation.fieldContactMode.value],
      address: {},
      createdBy: {
        name: this.formValidation.raisedBy.value,
        phoneNumber: this.formValidation.contactNumber.value,
        preferredContactMode: this.formValidation.modeOfContact.value
      }
    };
    this.httpService.createRequest(request).subscribe((response) => {
      this.logger.log(response);
      this.success = 'You help will reach you soon';
      setTimeout(() => {
        this.clearAlerts();
      }, 5000);
    }, (error) => {
      this.error = 'Sorry, Its our issue';
      setTimeout(() => {
        this.clearAlerts();
      }, 5000);
    });
    this.logger.log('Request data', request);
  }

  clearAlerts(){
    this.error = null;
    this.success = null;
  }

}
