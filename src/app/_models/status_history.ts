export class StatusHistory {
  comment: string;
  statusCode: string;
  subStatusCode: string;
  timestamp: number;
  voluntreer: string;
}
