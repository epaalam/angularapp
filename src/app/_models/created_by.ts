export class CreatedBy {
  name: string;
  phoneNumber: number;
  preferredContactMode: string[];
}
