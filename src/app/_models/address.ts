export class Address {
  latitude: number;
  longitude: number;
  streetAddress: string;
  pincode: number;
}
