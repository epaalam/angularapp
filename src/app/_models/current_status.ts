export class CurrentStatus {
  comment: string;
  statusCode: string;
  subStatusCode: string;
  timestamp: number;
  voluntreer: string;
}
