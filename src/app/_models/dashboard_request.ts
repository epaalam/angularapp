import { CreatedBy } from './created_by';
import { StatusHistory } from './status_history';
import { Update } from './update';
import { CurrentStatus } from './current_status';

export class DashboardRequest {
  address: string;
  createdBy: CreatedBy;
  currentStatus: CurrentStatus;
  phoneNumber: number;
  preferredContactMode: string[];
  requestDetails: string;
  requestType: string;
  statusHistory: StatusHistory[];
  updates: Update[];
}
