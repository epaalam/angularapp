export class Update {
  addedBy: string;
  comment: string;
  date: number;
}
