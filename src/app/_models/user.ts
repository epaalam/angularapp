export class User {
  userName: string;
  phonenumber: string;
  email: string;
  isApproved: boolean;
  isAdmin: boolean;
  serviceableAreas: [string];

  constructor(
    username: string,
    phonenumber: string,
    email: string,
    isApproved: boolean,
    isAdmin: boolean,
    serviceableAreas: [string]
  ) {
    this.userName = username;
    this.phonenumber = phonenumber;
    this.email = email;
    this.isApproved = isApproved;
    this.isAdmin = isAdmin;
    this.serviceableAreas = serviceableAreas;
  }
}
