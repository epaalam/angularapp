import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/app.login';
import { RegistrationComponent } from './registration/registration.component';
import { LoggerModule } from 'ngx-logger';
import { environment } from 'src/environments/environment';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AlertService } from './_services/alertservice';
import { HttpService } from './_services/httpservice';
import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { AuthenticationService } from './_services/authenticationService';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { RequestComponent } from './dashboard/request/request.component';
import { ApproveUserComponent } from './dashboard/approve-user/approve-user.component';
import { ProfileComponent } from './dashboard/profile/profile.component';
import { RaiseRequestComponent } from './raise-request/raise-request.component';
import {WindowService} from './_services/WindowService';
import * as firebase from 'firebase';

firebase.initializeApp(environment.firebase);
// @ts-ignore
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegistrationComponent,
    DashboardComponent,
    RequestComponent,
    ApproveUserComponent,
    ProfileComponent,
    RaiseRequestComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    LoggerModule.forRoot({
      serverLoggingUrl: `${environment.apiUrl}api/logs`,
      level: environment.logLevel,
      serverLogLevel: environment.serverLogLevel,
      disableConsoleLogging: false,
    }),
  ],
  providers: [
    AlertService,
    HttpService,
    AuthenticationService,
    WindowService,
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
