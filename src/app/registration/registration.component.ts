import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {WindowService} from '../_services/WindowService';
import * as firebase from 'firebase';
import {NGXLogger} from 'ngx-logger';
import {HttpService} from '../_services/httpservice';
import {Value} from '@angular/fire/remote-config';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
})
export class RegistrationComponent implements OnInit {
  registrationForm: FormGroup;
  otpForm: FormGroup;
  submitted = false;
  error: string;
  success: string;
  windowRef: any;
  recaptchaVerifier;
  confirmationResult;
  isSignIn = true;
  registrationDetails;
  phoneNumber = '';
  otp;

  constructor(
    private router: Router,
    public route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private otpFormBuilder: FormBuilder,
    private logger: NGXLogger,
    private win: WindowService,
    private httpService: HttpService
  ) { }

  ngOnInit() {
    this.registrationForm = this.formBuilder.group({
      name: ['', Validators.required],
      password: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      agreement: ['', Validators.required],
      modeOfContact: ['Phone'],
      email: [''],
      volunteer: [''],
      ngo: [''],
    });
    this.otpForm = this.otpFormBuilder.group({
      otp: ['', Validators.required]
    });
    this.error = null;
    this.success = null;
  }

  get formValidation() {
    return this.registrationForm.controls;
  }
  get otpFormValidation(){
    return this.otpForm.controls;
  }
  onSubmit() {
    this.logger.log('Form Submitted');
    this.submitted = true;
    this.error = null;
    this.success = null;
    if (this.registrationForm.invalid) {
      this.error = 'Please fill the required details';
      return;
    }
    if (this.formValidation.volunteer.value === '' && this.formValidation.ngo.value === '') {
      this.error = 'You should be either an NGO or Volunteer';
      return;
    }
    setTimeout(() => {
      this.windowRef = this.win.windowRef;
      this.windowRef.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
      this.windowRef.recaptchaVerifier.render().then( widgetId => {this.windowRef.recaptchaWidgetId = widgetId; });
    }, 0);
    setTimeout(() => {
      this.sendLoginCode();
    }, 2000);
  }
  onOtpSubmit(){
    this.logger.log('OTP Form Submitted');
    this.submitted = true;
    this.error = null;
    this.success = null;
    if (this.otpForm.invalid) {
      this.error = 'Please Enter the OTP';
      return;
    }
    this.otp = this.otpFormValidation.otp.value;
    this.verifyLoginCode();
  }

  sendLoginCode() {
    const appVerifier = this.windowRef.recaptchaVerifier;
    firebase.auth().signInWithPhoneNumber('+91' + this.formValidation.phoneNumber.value, appVerifier)
      .then(result => {
        console.log((result));
        this.windowRef.confirmationResult = result;
        this.isSignIn = false;
        this.registrationDetails = {
          name : this.formValidation.name.value,
          phoneNumber : '91' + this.formValidation.phoneNumber.value,
          preferredContactMode : [this.formValidation.modeOfContact.value],
          password : this.formValidation.password.value,
          email : this.formValidation.email.value,
          isVolunteer : this.formValidation.volunteer.value !== '',
          isNGO : this.formValidation.ngo.value !== '',
          serviceableAreas: {}
        };
        this.logger.log('Registration details', this.registrationDetails);
        this.phoneNumber = this.formValidation.phoneNumber.value;
        this.error = null;
        this.success = null;
        this.submitted = false;
      })
      .catch( error => {
        console.log(error);
        this.error = 'Invalid PhoneNumber';
      });
  }

  verifyLoginCode() {
    this.windowRef.confirmationResult
      .confirm(this.otp)
      .then( result => {
        console.log(result.user);
        this.error = null;
        this.success = null;
        this.httpService.signUp(this.registrationDetails).subscribe((response) => {
          this.logger.log(response);
          this.router.navigate(['/login']);
        }, (err) => {
          this.logger.error(err);
        });
      })
      .catch( error => {
        console.log(error, 'Incorrect code entered?');
        this.error = 'Otp id invalid';
      });
  }
  clearAlerts(){
    this.error = null;
    this.success = null;
  }
}
