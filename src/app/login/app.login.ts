import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../_services/authenticationService';
import { NGXLogger } from 'ngx-logger';
import {HttpService} from '../_services/httpservice';

@Component({
  selector: 'app-login',
  templateUrl: './app.login.html',
  styleUrls: ['./app.login.css'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;
  error: string;
  success: string;
  loginBody;
  user;

  constructor(
    private router: Router,
    public route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private logger: NGXLogger
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      phoneNumber: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  get formValidation() {
    return this.loginForm.controls;
  }

  onSubmit() {
    this.logger.log('Form Submitted');
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.logger.log(this.formValidation.phoneNumber.value, this.formValidation.password.value);
    this.loginBody = {
      phoneNumber: '+91' + this.formValidation.phoneNumber.value,
      password: this.formValidation.password.value
    };
    this.error = null;
    this.success = null;
    this.authenticationService.login(this.loginBody).subscribe((result) => {
      this.logger.log(result);
      this.router.navigate(['/dashboard/request']);
    }, (error) => {
      this.logger.log(error);
    });
  }

  openRegistration() {
    this.router.navigate(['/register']);
  }

  openPasswordReset() {
    this.router.navigate(['/forgotPassword']);
  }
}
