import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/_services/authenticationService';
import { Router } from '@angular/router';
import { NGXLogger } from 'ngx-logger';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(map(result => result.matches));


  constructor(
    private breakpointObserver: BreakpointObserver,
    private authenticationService: AuthenticationService,
    private router: Router,
    private logger: NGXLogger
  ) { }

  ngOnInit(): void {
  }

  navigateTo(routesID: string) {
    this.logger.log('Navigating to ', routesID);
    switch (routesID) {
      case 'request':
        this.router.navigate(['/dashboard/request']);
        break;
      case 'approve':
        this.router.navigate(['/dashboard/approveUsers']);
        break;
      case 'profile':
        this.router.navigate(['/dashboard/profile']);
        break;
      case 'logout':
        this.authenticationService.logout();
        break;
      default:
        this.router.navigate(['/dashboard/request']);
        break;
    }
  }

  refresh() {
  }

  logout() {
    this.authenticationService.logout();
  }


}
