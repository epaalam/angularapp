import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../_services/httpservice';
import { DashboardRequest } from '../../_models/dashboard_request';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {
  requests: DashboardRequest[];

  constructor(private httpService: HttpService) {
  }

  ngOnInit(): void {
    this.httpService.getAllRequests().subscribe(res => {
      console.log(res);
      this.requests = res;
    }, error => console.log(error));
  }

}
