import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/app.login';
import { RegistrationComponent } from './registration/registration.component';
import { AuthGuard } from './_helpers/auth.guard';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { RequestComponent } from './dashboard/request/request.component';
import { ApproveUserComponent } from './dashboard/approve-user/approve-user.component';
import { ProfileComponent } from './dashboard/profile/profile.component';
import {RaiseRequestComponent} from './raise-request/raise-request.component';

const routes: Routes = [
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: '**',
        component: RequestComponent,
        canActivate: [AuthGuard]
      },
      {
        path: 'request',
        component: RequestComponent,
        canActivate: [AuthGuard]
      }, {
        path: 'approveUsers',
        component: ApproveUserComponent,
        canActivate: [AuthGuard]
      }, {
        path: 'profile',
        component: ProfileComponent,
        canActivate: [AuthGuard]
      }
    ]
  },
  {
    path: 'register',
    component: RegistrationComponent,
  }, {
    path: 'login',
    component: LoginComponent,
  }, {
    path: '**',
    component: RaiseRequestComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule { }
