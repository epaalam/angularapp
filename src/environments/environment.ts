// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import { NgxLoggerLevel } from 'ngx-logger';

export const environment = {
  production: false,
  apiUrl: 'http://localhost:4200/',
  logLevel: NgxLoggerLevel.DEBUG,
  serverLogLevel: NgxLoggerLevel.OFF,
  firebase: {
    apiKey: 'AIzaSyAao8b7rLEMGiUP6chovJMqaYxAmjU-rXA',
    authDomain: 'long-setting-275219.firebaseapp.com',
    databaseURL: 'https://long-setting-275219.firebaseio.com',
    projectId: 'long-setting-275219',
    storageBucket: 'long-setting-275219.appspot.com',
    messagingSenderId: '453235427834',
    appId: '1:453235427834:web:da1dbfcd4b375c6107bc43',
    measurementId: 'G-QH686WXSPC'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
